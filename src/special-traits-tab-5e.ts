// Import TypeScript modules
import { registerSettings } from './module/settings';
import { preloadTemplates } from './module/preloadTemplates';
import { getToolProficiencies, addActor5eRollTool } from './scripts/utils';
import { MODULE_ID, debug, log, warn, error, i18n } from './module/constants';
//@ts-ignore
import { d20Roll } from "../../systems/dnd5e/module/dice.js";
//@ts-ignore
import { DND5E } from "../../systems/dnd5e/module/config.js";
//@ts-ignore
import ActorSheet5e from "../../systems/dnd5e/module/actor/sheets/base.js";
//@ts-ignore
import ActorSheet5eNPC from "../../systems/dnd5e/module/actor/sheets/npc.js";
//@ts-ignore
import ActorSheet5eCharacter from "../../systems/dnd5e/module/actor/sheets/character.js";

  const MODULE_NAME = MODULE_ID;

  /* ------------------------------------ */
  /* Initialize module					*/
  /* ------------------------------------ */
  Hooks.once('init', async function() {
    log(`${MODULE_ID} | Initializing ${MODULE_ID}`);

    // Assign custom classes and constants here
    
    // Register custom module settings
    registerSettings();
    
    // Preload Handlebars templates
    await preloadTemplates();

  });

  /* ------------------------------------ */
  /* Setup module							*/
  /* ------------------------------------ */
  Hooks.once('setup', function() {
    // Do anything after initialization but before
    // ready
    //patchActor5ePrepareData();
    //addActor5eRollTool();
    //patchActorSheet5eCharacterRenderInner();

    //initSheetTab();
    patchActor5ePrepareData();
    addActor5eRollTool();
    patchActorSheet5eCharacterRenderInner();
  });

  /* ------------------------------------ */
  /* When ready							*/
  /* ------------------------------------ */
  Hooks.once('ready', function() {
    game.settings.register(MODULE_NAME, "special-traits-5e-compendium", {
      name: i18n("special-traits-5e.trait-compendium.name"),
      hint: i18n("special-traits-5e.trait-compendium.hint"),
      scope: "world",
      config: true,
      isSelect: true,
      //type: Boolean,
      //default: false
      type: String,
      default: "dnd5e.traits"
      //default: "dnd5e.items",
      //choices: itemCompendiums,
    });

    const itemCompendiums = game.packs
      .filter((pack) => pack.entity === "Item")
      .reduce((choices, pack) => {
        choices[
          pack.collection
        ] = `[${pack.metadata.package}] ${pack.metadata.label}`;
        return choices;
    }, {});

    game.settings.register(MODULE_NAME, "tool-compendium", {
      name: "tool-proficiencies-5e.tool-compendium.name",
      hint: "tool-proficiencies-5e.tool-compendium.hint",
      scope: "world",
      config: true,
      type: String,
      isSelect: true,
      default: "dnd5e.items",
      choices: itemCompendiums,
    });

  });


  // export function initSheetTab() {
  //   Hooks.on(`renderItemSheet5e`, (app, html, data) => {
  //     addSpecialTraitsTab(app, html, data);
  //   });
  // }

  // Hooks.on("renderActorSheet5eCharacter", injectActorSheet);

  // Add any additional hooks if necessary

  Handlebars.registerHelper("ifEquals", function (arg1, arg2, options) {
    return arg1 == arg2 ? options.fn(this) : options.inverse(this);
  });

  function patchActor5ePrepareData() {
    const oldPrepareData = CONFIG.Actor.entityClass.prototype.prepareData;
  
    CONFIG.Actor.entityClass.prototype.prepareData = function () {
      try {
        oldPrepareData.call(this);
      } catch (e) { }
  
      const toolProficiencies = getToolProficiencies(this);
      const abilities = this.data.data.abilities;
      const prof = this.data.data.attributes.prof;
      toolProficiencies.forEach((tool) => {
        const toolData = this.getFlag(MODULE_NAME, tool) || {
          ability: "-",
          bonus: 0,
          expertise: false,
          modifier: "?",
          label: tool,
        };
  
        if (toolData.ability == "-") toolData.modifier = "?";
        else {
          let mod = abilities[toolData.ability].mod;
          let bonus = parseInt(toolData.bonus);
          toolData.modifier = (
            prof +
            mod +
            bonus +
            (toolData.expertise ? prof : 0)
          ).toString();
        }
        this.setFlag(MODULE_NAME, tool, toolData);
      });
    };
  }

  async function patchActorSheet5eCharacterRenderInner() {
    log("Patching ActorSheet5eCharacter.prototype._renderInner");
    const old_renderInner = ActorSheet5eCharacter.prototype._renderInner;
    ActorSheet5eCharacter.prototype._renderInner = async function(data, options) {
      const html = await old_renderInner.call(this, data, options);
      try {
        //await injectActorSheet(this.object, html);
        //await addSpecialTraitsTab(this, html, data);
        await addToolsTab(this, html, data);
      } catch (e) {
        error(e);
      }
      return html;
    }
  }

  async function addToolsTab(app,html,data) {
    let actor = app.object;
    const tabSelector = html.find(`.tabs`);// html.find(`nav.sheet-navigation.tabs`);
    const toolTabString = `<a class="item" data-group="primary" data-tab="toolsProficiencies">${i18n(
      "Tools"
    )}</a>`;
    tabSelector.append($(toolTabString));
  
    const sheetContainer = html.find(`.sheet-body`);
    const tabContainer = $(`<div class="tab" data-group="primary" data-tab="toolsProficiencies"></div>`);
    sheetContainer.append(tabContainer);
    
  
    const toolProficiencies = getToolProficiencies(actor).map((tool) =>
      actor.getFlag(MODULE_NAME, tool)
    );
    const compendium = await game.packs.find(
      (pack) =>
        pack.collection === game.settings.get(MODULE_NAME, "tool-compendium")
    );
    const index = compendium && (await compendium.getIndex());
  
    for (let toolData of toolProficiencies) {
      const item = index && index.find((i) => i.name == toolData.label);
      toolData.img = item
        ? (await compendium.getEntity(item._id)).img
        : "/systems/dnd5e/icons/items/equipment/gloves.png";
    }
  
    let template = await renderTemplate(
      `modules/${MODULE_NAME}/templates/tool-proficiencies.hbs`,
      {
        DND5E: CONFIG.DND5E,
        toolProficiencies,
        MODULE_NAME: MODULE_NAME
      }
    );
    tabContainer.append(template);
  
    toolProficiencies.forEach((tool) => {
      const row = html.find(
        `[data-tab="toolsProficiencies"] [data-item-id="${tool.label}"]`
      );
      row.find(".item-name").click((event) => {
        actor.rollTool(tool.label);
      });
    });
  } 

  //async function injectActorSheet(actor, html) {
  async function addSpecialTraitsTab(app,html,data) {
    //let item = app.object;
    let actor = app.object;
    const tabSelector = html.find(`.tabs`);// html.find(`nav.sheet-navigation.tabs`);
    const traitsTabString = `<a class="item" data-group="primary" data-tab="specialTraits">${i18n(
      "Traits"
    )}</a>`;
    const tab = tabSelector.append($(traitsTabString));

    let sheetContainer = html.find(`.sheet-body`);

    if (sheetContainer.length === 0) sheetContainer = html.find(`.primary-body`);

    const tabContainer = $(`<div class="tab" data-group="primary" data-tab="specialTraits"></div>`);
    
    let extraTab = sheetContainer.append(tabContainer);
    
    // intercept Input Click and set Tab
    /*
    html.find('.specialTraits input[type="checkbox"]').click(ev => {
      makeActive = true;
    });

    html.find('.specialTraits input[type="text"]').click(ev => {
      makeActive = true;
    });
    */
    /*
    const specialTraits = getSpecialTraits(actor).map((trait) =>
      actor.getFlag(MODULE_NAME, trait)
    );
    */
    let templateData = {
      config: CONFIG.DND5E,
      MODULE_NAME: MODULE_NAME,
      data: _getData(app)//actor.data.data
    }
    const template = `modules/${MODULE_NAME}/templates/actor-traits.hbs`;

    const templateHtml = await renderTemplate(template, templateData);

    tabContainer.append(templateHtml);

  }

  /* -------------------------------------------- */

  /**
   * Prepare data used to render the special Actor traits selection UI
   * @return {Object}
   */
  function _getData(app) {
    const dataActor = app.object.data.data;//super.getData();
    dataActor.actor = app.object;//this.object;
    dataActor.flags = _getFlags(app.object);
    dataActor.bonuses = _getBonuses(app.object);
    return dataActor;
  }

  /* -------------------------------------------- */

  /**
   * Prepare an object of flags data which groups flags by section
   * Add some additional data for rendering
   * @return {Object}
   */
  function _getFlags(entity) {
    const flags = {};
    for ( let [k2, v2] of Object.entries(CONFIG.DND5E.characterFlags) ) {
      let v: any = v2;
      let k: any = k2;
      if ( !flags.hasOwnProperty(v.section) ) flags[v.section] = {};
      let flag: any = duplicate(v);
      flag.type = v.type.name;
      flag.isCheckbox = v.type === Boolean;
      flag.isSelect = v.hasOwnProperty('choices');
      flag.value = entity.getFlag("dnd5e", k);
      flags[v.section][`flags.dnd5e.${k}`] = flag;
    }
    return flags;
  }

  /* -------------------------------------------- */

  /**
   * Get the bonuses fields and their localization strings
   * @return {Array}
   * @private
   */
  function _getBonuses(actor) {
    const bonuses = [
      {name: "data.bonuses.mwak.attack", label: "DND5E.BonusMWAttack", value: ""},
      {name: "data.bonuses.mwak.damage", label: "DND5E.BonusMWDamage", value: ""},
      {name: "data.bonuses.rwak.attack", label: "DND5E.BonusRWAttack", value: ""},
      {name: "data.bonuses.rwak.damage", label: "DND5E.BonusRWDamage", value: ""},
      {name: "data.bonuses.msak.attack", label: "DND5E.BonusMSAttack", value: ""},
      {name: "data.bonuses.msak.damage", label: "DND5E.BonusMSDamage", value: ""},
      {name: "data.bonuses.rsak.attack", label: "DND5E.BonusRSAttack", value: ""},
      {name: "data.bonuses.rsak.damage", label: "DND5E.BonusRSDamage", value: ""},
      {name: "data.bonuses.abilities.check", label: "DND5E.BonusAbilityCheck", value: ""},
      {name: "data.bonuses.abilities.save", label: "DND5E.BonusAbilitySave", value: ""},
      {name: "data.bonuses.abilities.skill", label: "DND5E.BonusAbilitySkill", value: ""},
      {name: "data.bonuses.spell.dc", label: "DND5E.BonusSpellDC", value: ""}
    ];
    for ( let b of bonuses ) {
      //b.value = getProperty(object.data, b.name) || "";
      b.value = getProperty(actor.data, b.name) || "";
    }
    return bonuses;
  }

  /* -------------------------------------------- */

  /**
   * Update the Actor using the configured flags
   * Remove/unset any flags which are no longer configured
   */
  async function _updateObject(event, formData) {
    const actor = this.object;
    let updateData = expandObject(formData);

    // Unset any flags which are "false"
    let unset = false;
    const flags = updateData.flags.dnd5e;
    for ( let [k2, v2] of Object.entries(flags) ) {
      let v: any = v2;
      let k: any = k2;
      if ( [undefined, null, "", false, 0].includes(v) ) {
        delete flags[k];
        if ( hasProperty(actor.data.flags, `dnd5e.${k}`) ) {
          unset = true;
          flags[`-=${k}`] = null;
        }
      }
    }

    // Clear any bonuses which are whitespace only
    for ( let b of Object.values(updateData.data.bonuses ) ) {
      for ( let [k, v] of Object.entries(b) ) {
        b[k] = v.trim();
      }
    }

    // Diff the data against any applied overrides and apply
    // TODO: Remove this logical gate once 0.7.x is release channel
    if ( !isNewerVersion("0.7.1", game.data.version) ){
      updateData = diffObject(this.object.data, updateData);
    }
    await actor.update(updateData, {diff: false});
  }
