import { MODULE_ID } from '../module/constants.js';

export const preloadTemplates = async function() {
	const templatePaths = [
		// Add paths to "modules/xxxx/templates/yyyy.hbs"
		// TODO verify if i need
		`modules/${MODULE_ID}/templates/actor-traits.hbs`,
	];

	return loadTemplates(templatePaths);
}
