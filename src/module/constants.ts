
export const MODULE_ID = 'special-traits-tab-5e';

export enum MySettings {
  showACModifier = 'showACModifier',
}

export let debugEnabled = 0;
// 0 = none, warnings = 1, debug = 2, all = 3
export let debug = (...args) => {if (debugEnabled > 1) console.log(`DEBUG: ${MODULE_ID} | `, ...args)};
export let log = (...args) => console.log(`${MODULE_ID} | `, ...args);
export let warn = (...args) => {if (debugEnabled > 0) console.warn(`${MODULE_ID} | `, ...args)};
export let error = (...args) => console.error(`${MODULE_ID} | `, ...args)
export let i18n = key => {
return game.i18n.localize(key);
};
export let setDebugLevel = (debugText: string) => {
  debugEnabled = {"none": 0, "warn": 1, "debug": 2, "all": 3}[debugText] || 0;
  // 0 = none, warnings = 1, debug = 2, all = 3
  CONFIG.debug.hooks = debugEnabled >= 3;
}

// const specialTraitsCodeMapping = {
//     disg: "Disguise Kit",
//     forg: "Forgery Kit",
//     herb: "Herbalism Kit",
//     navg: "Navigator's Tools",
//     pois: "Poisoner's Kit",
//     thief: "Thieves' Tools",
//   };
  

// export const getSpecialTraits = (actor) => {
//     return actor.data.data.traits.toolProf ? 
//         actor.data.data.traits.toolProf.value.map(v => specialTraitsCodeMapping[v])
//             .filter(v => v!= null)
//             .concat(actor.data.data.traits.toolProf.custom.split(";")) 
//         : []; 
// }