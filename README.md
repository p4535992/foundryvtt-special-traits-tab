# special-traits-tab-5e

## What it does

This module adds a new tab to the actor sheet which allows the user to set any traits proficiencies declared on their character sheet.
Because i find a little "hard" to find and read the standard trait div and i want o add more custom feature

# Versions

- **0.0.1 (First try) ** 

  - Implemented tab injection into character actor sheets.
  - In this tab you will see all traits you have proficiencies, even cusotm added ones in the tool proficiencies section (the input marked with `Special`).
  - The traits tab will show all your traits with which you are proficient and it allows you to customize the proficiency, eg. if you have expertise in it (useful for artificers), if you have a custom bonus (ie. maybe some magical item tool) or which attribute to use when rolling the tool (if none is selected, you are queried when rolling).
  - The traits can be rolled by clicking on their icon.
  - The traits will automatically take the icon of the item with the same name as the traits in the configured compendium in settings.
  - Patched `Actor5e.prorotype.prepareData` to add necesary flags on actor.
  
# TODO

  - Added `rollTool` function to `Actor5e.prototype`. It can accept any trait as a string. If the actor doesn't have proficiency in the trait you passed, it will roll without proficiency (or half proficiency if you have Jack of all trades). `rollTool` behaves similarly to `rollSkill`.
  - Possible intgraiton with Active Effects
